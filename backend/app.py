from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from sqlalchemy import Column, func, or_
from sqlalchemy import (
    Integer, String, DateTime, Text,
    ForeignKey, text
)
import os

backend_path = os.path.dirname(os.path.abspath(__file__))
db_file_path = os.path.join(backend_path, "db.sqlite3")

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{db_file_path}"
db = SQLAlchemy(app)


class VideoMeasurement(db.Model):
    __tablename__ = 'video_measurement'

    id = Column(Integer, primary_key=True, autoincrement=True)
    video_id = Column(Integer, ForeignKey('video.id', ondelete="CASCADE"))
    video = relationship("Video", back_populates="measurements")
    measurement_date = Column(DateTime())
    sub_count = Column(Integer, server_default=text("0"))
    comments = Column(Integer, server_default=text("0"))
    subscribersgained = Column(Integer, server_default=text("0"))
    subscriberslost = Column(Integer, server_default=text("0"))
    unsub_views = Column(Integer, server_default=text("0"))
    unsub_likes = Column(Integer, server_default=text("0"))
    unsub_dislikes = Column(Integer, server_default=text("0"))
    unsub_shares = Column(Integer, server_default=text("0"))

    def as_json(self):
        return {
            'id': self.id,
            'video_id': self.video_id,
            'measurement_date': self.measurement_date.isoformat(),
            'comments': self.comments,
            'subscribersgained': self.subscribersgained,
            'subscriberslost': self.subscriberslost,
            'unsub_views': self.unsub_views,
            'unsub_likes': self.unsub_likes,
            'unsub_dislikes': self.unsub_dislikes,
            'unsub_shares': self.unsub_shares,
        }


class Video(db.Model):
    __tablename__ = 'video'

    id = Column(Integer, primary_key=True, autoincrement=True)
    youtube_id = Column(String(128))
    channel_id = Column(Integer, ForeignKey('channel.id'))
    channel = relationship("Channel", back_populates="videos")
    create_date = Column(DateTime())
    title = Column(String(128))
    description = Column(Text())
    duration = Column(Integer)
    measurements = relationship(
        "VideoMeasurement", cascade="all,delete",
        back_populates="video", passive_deletes=True)


class Channel(db.Model):
    __tablename__ = 'channel'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(128))
    videos = relationship("Video")


# Display video measurements grouped by channel and video
@app.route('/video_measurement', methods=['GET'])
def video_measurement():
    results = db.session.query(VideoMeasurement)\
                .join(Video)\
                .join(Channel)\
                .group_by(
                    Video.id,
                    Channel.id
                )\
                .all()

    return (jsonify([result.as_json() for result in results]))


# Each video has multiple measurements. We only want to display the LATEST 
# video measurement (limit 1) per video
@app.route('/latest_video_measurement', methods=['GET'])
def latest_video_measurement():
    subquery = db.session.query(
                    VideoMeasurement,
                    func.rank().over(
                        partition_by=VideoMeasurement.video_id,
                        order_by=VideoMeasurement.measurement_date.desc()
                    ).label('rnk')
                ).subquery()
    results = db.session.query(subquery).filter(subquery.c.rnk==1)

    return jsonify([{
                'id': result.id,
                'video_id': result.video_id,
                'measurement_date': result.measurement_date.isoformat(),
                'comments': result.comments,
                'subscribersgained': result.subscribersgained,
                'subscriberslost': result.subscriberslost,
                'unsub_views': result.unsub_views,
                'unsub_likes': result.unsub_likes,
                'unsub_dislikes': result.unsub_dislikes,
                'unsub_shares': result.unsub_shares,
                'rnk': result.rnk
            } for result in results
        ]
    )


# Create backend API endpoint to retrieve the data in any way you want
# header "Content-Type": "application/json"
# body  {
#           "key": "value"
#       }
@app.route('/retrieve_data', methods=['POST'])
def retrieve_data():
    data = request.get_json()
    search_pattern = data.get("key")

    records = db.session.query(Video, VideoMeasurement, Channel)\
                .join(VideoMeasurement)\
                .join(Channel)\
                .filter(or_(
                    # condition of the Video table
                    Video.id.like("%" + search_pattern + "%"),
                    Video.youtube_id.like("%" + search_pattern + "%"),
                    Video.create_date.like("%" + search_pattern + "%"),
                    Video.title.like("%" + search_pattern + "%"),
                    Video.description.like("%" + search_pattern + "%"),
                    Video.duration.like("%" + search_pattern + "%"),

                    # condition of the VideoMeasurement table
                    VideoMeasurement.id.like("%" + search_pattern + "%"),
                    VideoMeasurement.measurement_date.like("%" + search_pattern + "%"),
                    VideoMeasurement.sub_count.like("%" + search_pattern + "%"),
                    VideoMeasurement.comments.like("%" + search_pattern + "%"),
                    VideoMeasurement.subscribersgained.like("%" + search_pattern + "%"),
                    VideoMeasurement.subscriberslost.like("%" + search_pattern + "%"),
                    VideoMeasurement.unsub_views.like("%" + search_pattern + "%"),
                    VideoMeasurement.unsub_likes.like("%" + search_pattern + "%"),
                    VideoMeasurement.unsub_dislikes.like("%" + search_pattern + "%"),
                    VideoMeasurement.unsub_shares.like("%" + search_pattern + "%"),

                    # condition of the Channel table
                    Channel.id.like("%" + search_pattern + "%"),
                    Channel.name.like("%" + search_pattern + "%"),
                ))\
                .group_by(Video.id)\
                .all()

    result_data = []
    for record in records:
        result_data.append({
            # Video information
            'video_create_date': record[0].create_date.isoformat(),
            'video_title': record[0].title,
            'video_description': record[0].description,
            'video_duration': record[0].duration,

            # VideoMeasurement information
            'video_measurement_measurement_date': record[1].measurement_date\
                .isoformat(),
            'video_measurement_comments': record[1].comments,
            'video_measurement_subscribersgained': record[1].subscribersgained,
            'video_measurement_subscriberslost': record[1].subscriberslost,
            'video_measurement_unsub_views': record[1].unsub_views,
            'video_measurement_unsub_likes': record[1].unsub_likes,
            'video_measurement_unsub_dislikes': record[1].unsub_dislikes,
            'video_measurement_unsub_shares': record[1].unsub_shares,

            # Channel information
            'channel_name': record[2].name,
        })

    return jsonify({"data": result_data})


if __name__ == '__main__':
    app.run()
