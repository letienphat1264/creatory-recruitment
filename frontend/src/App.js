import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css';

import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import { MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';

function App() {
  const [apiVideoMeasurementData, setApiVideoMeasurementData] = useState([]);
  const [isVideoMeasurementLoading, setIsVideoMeasurementLoading] = useState(true);
  const [isVideoMeasurementError, setIsVideoMeasurementError] = useState(false);

  const [apiLatestVideoMeasurementData, setApiLatestVideoMeasurementData] = useState([]);
  const [isLatestVideoMeasurementLoading, setIsLatestVideoMeasurementLoading] = useState(true);
  const [isLatestVideoMeasurementError, setIsLatestVideoMeasurementError] = useState(false);

  useEffect(() => {
    const fetchVideoMeasurementData = async () => {
        setIsVideoMeasurementLoading(true);
        setIsVideoMeasurementError(false);
        try {
          const video_measurement = await axios(
            '/video_measurement'
          );
          setIsVideoMeasurementLoading(false);
          setApiVideoMeasurementData(video_measurement.data);
        }
        catch (error) {
          setIsVideoMeasurementLoading(false);
          setIsVideoMeasurementError(true);
        }
    };

    const fetchLatestVideoMeasurementData = async () => {
        setIsLatestVideoMeasurementLoading(true);
        setIsLatestVideoMeasurementError(false);
        try {
          const latest_video_measurement = await axios(
            '/latest_video_measurement'
          );
          setIsLatestVideoMeasurementLoading(false);
          setApiLatestVideoMeasurementData(latest_video_measurement.data);
        }
        catch (error) {
          setIsLatestVideoMeasurementLoading(false);
          setIsLatestVideoMeasurementError(true);
        }
    };

    fetchVideoMeasurementData();
    fetchLatestVideoMeasurementData();
  }, []);

  return (
    <div className="App">
      <h2 className="mt-3">Requirements</h2>
      <ul>
        <li>
          Display video measurements grouped by channel and video
        </li>
        <li>
          Each video has multiple measurements. We only want to display the LATEST video measurement (limit 1) per video
        </li>
        <li>
          Create backend API endpoint to retrieve the data in any way you want
        </li>
        <li>
          Visualize the frontend how you see fit
        </li>
      </ul>

      <h2 className="mt-3">Implementation</h2>
      <ul>
        <li>
          Display video measurements grouped by channel and video
        </li>
      </ul>
      <MDBTable striped bordered hover responsive>
        <MDBTableHead>
          <tr>
            <th>video_id</th>
            <th>measurement_date</th>
            <th>comments</th>
            <th>subscribersgained</th>
            <th>subscriberslost</th>
            <th>unsub_views</th>
            <th>unsub_likes</th>
            <th>unsub_dislikes</th>
            <th>unsub_shares</th>
          </tr>
        </MDBTableHead>
        <MDBTableBody>
          {
            apiVideoMeasurementData.map(video_measurement => (
              <tr key={video_measurement.id}>
                <td>{video_measurement.video_id}</td>
                <td>{video_measurement.measurement_date}</td>
                <td>{video_measurement.comments}</td>
                <td>{video_measurement.subscribersgained}</td>
                <td>{video_measurement.subscriberslost}</td>
                <td>{video_measurement.unsub_views}</td>
                <td>{video_measurement.unsub_likes}</td>
                <td>{video_measurement.unsub_dislikes}</td>
                <td>{video_measurement.unsub_shares}</td>
              </tr>
            ))
          }
        </MDBTableBody>
      </MDBTable>

      <ul>
        <li>
          Each video has multiple measurements. We only want to display the 
          LATEST video measurement (limit 1) per video
        </li>
      </ul>
      <MDBTable striped bordered hover responsive>
        <MDBTableHead>
          <tr>
            <th>video_id</th>
            <th>measurement_date</th>
            <th>comments</th>
            <th>subscribersgained</th>
            <th>subscriberslost</th>
            <th>unsub_views</th>
            <th>unsub_likes</th>
            <th>unsub_dislikes</th>
            <th>unsub_shares</th>
          </tr>
        </MDBTableHead>
        <MDBTableBody>
          {
            apiLatestVideoMeasurementData.map(latest_video_measurement => (
              <tr key={latest_video_measurement.id}>
                <td>{latest_video_measurement.video_id}</td>
                <td>{latest_video_measurement.measurement_date}</td>
                <td>{latest_video_measurement.comments}</td>
                <td>{latest_video_measurement.subscribersgained}</td>
                <td>{latest_video_measurement.subscriberslost}</td>
                <td>{latest_video_measurement.unsub_views}</td>
                <td>{latest_video_measurement.unsub_likes}</td>
                <td>{latest_video_measurement.unsub_dislikes}</td>
                <td>{latest_video_measurement.unsub_shares}</td>
              </tr>
            ))
          }
        </MDBTableBody>
      </MDBTable>
    </div>
  );
}

export default App;
